from functools import reduce


def factorial(n: int) -> int:
    reducer = lambda x, y: x * y
    return reduce(reducer, range(1, n + 1))


def main():
    try:
        number = int(input("Choose a number: "))
    except ValueError:
        print("This is not a number!")
        return

    print(f"{number}! = {factorial(number)}")


if __name__ == "__main__":
    main()
